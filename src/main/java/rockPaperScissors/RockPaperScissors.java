package rockPaperScissors;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Random;

import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
	private static ByteArrayOutputStream outputStream;   
    public void run() {
        // TODO: Implement Rock Paper Scissors
		String prompt = "Your choice (Rock/Paper/Scissors)?";
    	while(true) {
        	System.out.println("Let's play round " + roundCounter++);
        	
        	// read and validate input from user
        	String user_input = validate_input(prompt);
        	
        	// computer choose one random item from rpsChoices
        	Random ran = new Random();
        	int rand_int = ran.nextInt(rpsChoices.size());
        	String com_input = rpsChoices.get(rand_int);
        	
        	// check results then accumulate score and print results, 
        	int result = human_win(user_input, com_input);
        	if(result == 1) {
        		humanScore++;
        		System.out.println("Human chose " + user_input + ", computer chose " + com_input + ". Human wins!" );
        	}
        	else if(result == -1) {
        		computerScore++;
        		System.out.println("Human chose " + user_input + ", computer chose " + com_input + ". Computer wins!" );
        	}
        	else
        		System.out.println("Human chose " + user_input + ", computer chose " + com_input + ". It's a tie!" );

        	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        	System.out.println("Do you wish to continue playing? (y/n)?");
        	String choose = sc.next();
        	if(choose.equals("n")) {
        		System.out.println("Bye bye :)");
        		break;
        	}
    	}
    }
    
    /**
     * validate the input from readInput, if validated, return it, otherwise ask for input again.
     * @param prompt
     * @return
     */
    private String validate_input(String prompt) {
    	String user_input = null;
    	while(true) {
        	user_input = readInput(prompt);
        	if(rpsChoices.contains(user_input)) break;
        	else System.out.println("I do not understand " + user_input + ". Could you try again?"); 
    	}
    	return user_input;
    }

    /**
     * check result whether human win, loss or tie
     * @param human_input
     * @param computer_input
     * @return return 1 if human win, 0 if tie, -1 if loss
     */
    private Integer human_win(String human_input, String computer_input) {
    	int result = -999;
    	if(human_input.equals(computer_input)) {
    		result = 0;
    	}
    	else { 
        	if(human_input.equals("paper")) {
        		if(computer_input.equals("rock")) result = 1;
        		else result = -1;
        	}
        	else if(human_input.equals("rock")) {
        		if(computer_input.equals("scissors")) {
        			result = 1;
        		}
        		else {
        			result = -1;
        		}
        	}
        	else { // human_input == "scissors"
        		if(human_input.equals("paper")) result = 1;
        		else result = -1;
        	}	
    	}
    	return result;
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
